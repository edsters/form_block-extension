<?php namespace Emange\FormBlockExtension;

use Anomaly\BlocksModule\Block\BlockExtension;
use Anomaly\BlocksModule\Block\Command\AddConfigurationForm;
use Anomaly\BlocksModule\Block\Command\AddStreamForm;
use Anomaly\BlocksModule\Block\Form\BlockInstanceFormBuilder;
use Illuminate\Config\Repository;

class FormBlockExtension extends BlockExtension
{

    protected $provides = 'anomaly.module.blocks::block.pyroui_form_block';

    protected $view = 'emange.extension.form_block::content';

    protected $category = 'layout';

    //     Customize the multi form builder for this block
    public function extend(BlockInstanceFormBuilder $builder)
    {
        $config = app(Repository::class);

        $this->dispatch(new AddStreamForm($builder, $this));
        $this->dispatch(new AddConfigurationForm($builder, $this));

        $sections = $config->get($this->getNamespace('sections'));
        $builder->mergeSections($sections);

        $builder->prefixSectionFields($builder->getOption('prefix'));
    }

}
