<?php
return [
    'form'   => [
        'type'     => 'anomaly.field_type.relationship',
        'config'   => [
            'related' => 'Anomaly\FormsModule\Form\FormModel'
        ],
        'required' => TRUE
    ],
    'layout' => [
        'type'     => 'anomaly.field_type.editor',
        'config'   => [
            "default_value" => '{{ block_form.render()|raw}}',
        ],
        'required' => TRUE
    ],
];