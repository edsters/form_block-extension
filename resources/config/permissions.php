<?php

return [
    'block' => [
        'read',
        'write',
        'delete',
    ],
];
