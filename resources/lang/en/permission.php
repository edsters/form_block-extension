<?php

return [
    'block' => [
        'name'   => 'Block',
        'option' => [
            'read'   => 'Can read block?',
            'write'  => 'Can create/edit block?',
            'delete' => 'Can delete block?',
        ],
    ],
];
