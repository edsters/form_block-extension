<?php

return [
    'form'          => [
        'name'         => 'Form',
        'instructions' => 'The form we want to use'
    ],
    'layout'        => [
        'name'         => 'Form Layout',
        'instructions' => 'Define how we want to render the form.',
        'warning'      => 'You can access the form object with `form`'
    ]
];
